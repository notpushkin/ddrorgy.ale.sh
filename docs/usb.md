# Флешки

<div class="md-has-sidebar" markdown>
<aside markdown>

<img src="/images/vladyan.jpg" style="width: 4em; border-radius: 50%; vertical-align: middle;"><br>
[**Владян Кимченынович**](https://t.me/t_brk1)<br>
настроил и рассказал как это работает

</aside>
<main markdown>

Флешки работают достаточно хорошо, чтобы ими пользоваться, так что вот
мини-инструкция, как играть свои песни на автомате:

1. Создать папку на флешке, назвать её `StepMania 5.1`.

2. Cоздать в нашей папке ещё одну папку под названием `Songs`.

3. В эту папку кинуть именно песни (папка с `.sm`/`.ssc`, `.mp3` и т. д.) вместо «пачек»
   с песнями. Кинул картинку, как это должно выглядеть:

    ![](/images/usb-screenshot.jpg)

4. Прийти поиграть, вставить флешку и сыграть один кредит. После этого
   степмания сделает нужные файлы в нашей папке и на следующем кредите в меню
   песен будут ваши песни, скорее всего в пачке P1 или P2.

</main>
</div>

Стоит про это ещё пару штук сказать:

- На выборе песни флешку надо вставлять во время простоя / на выборе режима игры. Во время выбора песни вставлять и менять флешки пока что нельзя

- Несмотря на то, что порта два, левый и правый, компу все равно и первая вставленная флешка всегда будет первого (левого) игрока. А как сделать как надо я хз ¯\\\_(ツ)\_/¯
